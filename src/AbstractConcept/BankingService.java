package AbstractConcept;

public abstract class BankingService {

	public abstract void smsService();
		
	public abstract int balanceEnquiry();
	
	public abstract int cashWithdrawal(int currentbal, int amount);
	
	

}
