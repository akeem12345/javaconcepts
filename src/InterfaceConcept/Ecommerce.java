package InterfaceConcept;

public interface Ecommerce {
	
	public abstract String memberDetails(String memberName);
	
	public abstract int purchaseorder(int noofOrders); 

}
