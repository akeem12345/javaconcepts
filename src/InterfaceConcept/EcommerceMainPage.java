package InterfaceConcept;

public class EcommerceMainPage {
	public static void main(String[] args) {
		Flipkart fp = new Flipkart();
		String username= fp.memberDetails("Akeem");
		System.out.println("Flipkart username is: "+username);
		
		Ecommerce ecom = new Flipkart();
		ecom.memberDetails("Mohmmad");
		
		ecom = new Amazon();
		int orders =ecom.purchaseorder(5);
		System.out.println("No of Orders is: "+orders);
		
	}

}
