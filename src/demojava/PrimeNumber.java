package demojava;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number");
		int a = sc.nextInt();
		int b = 0;//counts the number of factors
		for(int i=1;i<=a;i++) {
			if(a%i ==0)//checking the factors of number
				b++;
		}
		if(b == 2) {
			System.out.println( a+ " is a prime number");
		}
		else {
			System.out.println( a+ " is not a prime number");
		}
	}

}
