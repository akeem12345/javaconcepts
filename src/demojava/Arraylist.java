package demojava;

import java.util.Arrays;

public class Arraylist {

	public static void main(String[] args) {
		int[] cs = new int[3];
		cs[0]=1;
		cs[1]=6;
		cs[2]=3;
		System.out.println(Arrays.toString(cs));
		System.out.println(" value stored at 1st Index list is: " +cs[1]);
		for(int i=0;i<cs.length;i++) {
			System.out.println(" value stored in " +i+ " index is " +cs[i]);
		}
		for(int as: cs) {
			System.out.println(as);
		}
		

	}

}
