package demojava;

import java.util.Scanner;

public class SwitchtheChoice {

	public static void main(String[] args) {
		int a,b;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a val of a and b");
		a=sc.nextInt();
		b=sc.nextInt();
		System.out.println(" 1.add\n 2.sub\n 3.mul\n 4.div\n select your choice");
		int choice = sc.nextInt();
		switch(choice) {
		case 1:
			System.out.println("adding");
			System.out.println(a+b);
			break;
		case 2:
			System.out.println("subtraction");
			System.out.println(a-b);
			break;
		case 3:
			System.out.println("multiple");
			System.out.println(a*b);
			break;
		case 4:
			System.out.println("divison");
			System.out.println(a/b);
			break;
		default :
			System.out.println("Invalid data");
			
		}
		sc.close();
	}

}
