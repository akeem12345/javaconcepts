package demojava;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {
		
		int b,c;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number");
		int n = sc.nextInt();
		
		int d=0;
		b=1;
		c=1;
		System.out.print("1 1 ");
		while(d<=n)
		{
			d=b+c;
			System.out.print(d+  "  "); 
			b=c;
			c=d;
			
		}

	}

}
