package demojava;

public class MultidimensionArray {

	public static void main(String[] args) {
		int[][] str = {{2,3,4},{4,5,6},{6,7,8}};
		
		System.out.println("total  no of rows " +str.length);
		
		for(int i=0;i<str.length;i++) 
		{
			for(int j=0;j<str[i].length;j++) 
			{
				System.out.print(str[i][j]+ "\t");
			}
			System.out.println();
		} 
		
	}

}
