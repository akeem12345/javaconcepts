package demojava;

import java.util.Scanner;

public class Swapping {

	public static void main(String[] args) {
		int a,b;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a val of a and b");
		a=sc.nextInt();
		b=sc.nextInt();
		System.out.println("before swap");
		System.out.println("value of a:"+a+"value of b:"+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("after swap");
		System.out.println("value of a:"+a+"value of b:"+b);

	}

}
