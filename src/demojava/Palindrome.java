package demojava;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number");
		int a = sc.nextInt();
		int b = a;
		int rev = 0;
		while(b!=0) 
		{
			int c = b%10;
			rev = 10*rev +c;
			b = b/10;
		}
		if(rev==a)
		{
			System.out.println( a+ " it is a palindrome");
			
		}
		else
		{
			System.out.println( a+ " it is not a palindrome");
		}

	}

}
